import React from 'react'
import { connect } from 'react-redux'
import { inject } from '@nrg/react-di'
import theme from './StatusBar.module.scss'

const Component = class extends React.Component {

  onClear = () => {
    this.props.controller.clearFileList()
  }

  render () {
    const {files, status: {completed, total}} = this.props

    return files.length ? (
      <div className={theme.default}>
        <span>Files Completed: {completed}/{total}</span>
        <button title="Clear all Uploads" onClick={this.onClear}>
          Clear
        </button>
      </div>
    ) : null
  }
}

const mapStateToProps = ({files, status}) => ({files, status})
const dependencies = {controller: 'controller'}

export default inject(connect(mapStateToProps)(Component), dependencies)