import React from 'react'
import { inject } from '@nrg/react-di'
import { Link, withRouter } from 'react-router-dom'

const Component = class extends React.Component {

  logout = () => {
    const {authControl, history} = this.props

    authControl.logout()
    history.push('/')
  }

  render () {
    const {authControl} = this.props

    return authControl.isGuest ?
      <Link to="/login">Login</Link> :
      <button onClick={this.logout}>Logout</button>
  }
}

const dependencies = {authControl: 'authControl'}

export default withRouter(inject(Component, dependencies))