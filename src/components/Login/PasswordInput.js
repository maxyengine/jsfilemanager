import React from 'react'

export default ({name, label, value, error, ...rest}) => {
  return (
    <div>
      <label>{label}</label>
      <input
        type={'password'}
        name={name}
        value={value}
        {...rest}
      />
      <div>{error}</div>
    </div>
  )
}
