import React from 'react'
import { inject } from '@nrg/react-di'
import { connect } from 'react-redux'
import theme from './UploadedFile.module.scss'

const Component = class extends React.Component {

  constructor (props) {
    super(props)
    this.client = this.props.client
    this.controller = this.props.controller
  }

  render () {
    const {file} = this.props
    const name = file.path.fileName.value

    return (
      <li className={`${theme.default} ${theme.success}`}>
        <div className={theme.inner}>
          <div className={theme.progress} style={{width: `${this.percent}%`}}/>
          <div className={theme.name} title={name}>{name}</div>
          <div className={theme.size}>{file.size.toHumanString()}</div>
          <div className={theme.controls}>
            <button className={theme.btnView} title="View" onClick={this.onView}>
              <i className="nrg-view"/>
            </button>
            <button className={theme.btnClose} title="Cancel" onClick={this.onClose}>
              <i className="nrg-across"/>
            </button>
          </div>
        </div>
      </li>
    )
  }
}

const mapStateToProps = (state) => ({})
const dependencies = {
  client: 'client',
  controller: 'controller'
}

export default inject(connect(mapStateToProps)(Component), dependencies)