import React from 'react'
import { inject } from '@nrg/react-di'
import theme from './App.module.scss'
import DragArea from '../DragArea'
import StatusBar from '../StatusBar'
import FileList from '../FileList'
import Loading from './Loading'
import Error from './Error'

const Component = class extends React.Component {

  state = {
    isReady: false,
    error: null
  }

  constructor (props) {
    super(props)
    this.controller = this.props.controller
  }

  async componentDidMount () {
    try {
      await this.controller.loadConfig()
      this.setState({isReady: true})
    } catch (error) {
      this.setState({error})
    }
  }

  render () {
    const {isReady, error} = this.state

    if (error) {
      return <Error error={error}/>
    }

    if (isReady) {
      return (
        <div className={theme.default}>
          <DragArea/>
          <StatusBar/>
          <FileList/>
        </div>
      )
    }

    return <Loading/>
  }
}

const dependencies = {controller: 'controller'}

export default inject(Component, dependencies)
