import React from 'react'
import { connect } from 'react-redux'
import theme from './FileList.module.scss'
import FileItem from '../FileItem'

const Component = class extends React.Component {

  render () {
    const {files} = this.props

    return (
      <div className={theme.default}>
        <ul>
          {files.map(file => <FileItem key={file._id} file={file}/>)}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = ({files}) => ({files})

export default connect(mapStateToProps)(Component)