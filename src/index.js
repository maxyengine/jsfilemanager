import '@babel/polyfill'
import 'whatwg-fetch'
import './styles/_fonts.scss'
import React from 'react'
import ReactDOM from 'react-dom'
import DemoClient from './services/demo/Client'
import DemoUploaderFactory from './services/demo/UploaderFactory'
import Session from './services/Session'
import AuthControl from './services/AuthControl'
import Controller from './services/Controller'
import Client from './services/Client'
import FileFactory from './services/FileFactory'
import Home from './pages/Home'
import UploadsFolder from './pages/UploadsFolder'
import Login from './components/Login'
import AuthBar from './components/AuthBar'
import PrivateRoute from './components/PrivateRoute'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { ServiceLocator, createInjector } from '@nrg/react-di'
import { Endpoint } from '@nrg/http'
import { Value } from '@nrg/core'
import { apiUrl } from './devConfig'

var __NRG_DEMO_MODE__ = false

window.$nrg = window.$nrg || {}
var $nrg = window.$nrg

$nrg.Uploader = $nrg.Uploader || class extends Value {

  get defaults () {
    return {
      apiUrl: '.',
      wrapper: document.body
    }
  }

  run () {
    const initState = {
      config: {},
      files: [],
      status: {
        total: 0,
        completed: 0
      }
    }

    const store = createStore(
      (state = initState, action) => action.state || state,
      composeWithDevTools())

    const injector = createInjector(__NRG_DEMO_MODE__ ?
      {
        store: store,
        controller: Controller,
        client: DemoClient,
        uploaderFactory: DemoUploaderFactory,
      } : {
        endpoint: [Endpoint, {
          apiUrl: this.apiUrl
        }],
        store: store,
        controller: Controller,
        client: Client,
        session: [Session, {name: '$nrg.Uploader'}],
        authControl: AuthControl,
        fileFactory: FileFactory
      })

    const app = (
      <Router>
        <ServiceLocator injector={injector}>
          <Provider store={store}>

            <Link to="/">Home</Link>
            <Link to="/uploads-folder">Uploads Folder</Link>

            <AuthBar/>

            <PrivateRoute exact path="/" component={Home}/>
            <PrivateRoute exact path="/uploads-folder" component={UploadsFolder}/>
            <Route path="/login" component={Login}/>

          </Provider>
        </ServiceLocator>
      </Router>
    )

    ReactDOM.render(app, this.wrapper)
  }
}

new $nrg.Uploader({
  apiUrl: apiUrl,
  wrapper: document.getElementById('app')
}).run()
