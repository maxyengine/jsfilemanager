import React from 'react'
import { inject } from '@nrg/react-di'
import { connect } from 'react-redux'
import theme from './UploadsFolder.module.scss'
import UploadedFile from '../../components/UploadedFile'
import Page from '../../components/Page'

const Component = class extends React.Component {

  state = {
    isReady: false,
    error: null,
    directory: null
  }

  constructor (props) {
    super(props)
    this.client = this.props.client
    this.controller = this.props.controller
  }

  async componentDidMount () {
    try {
      const directory = await this.client.fetchUploadsFolder()
      this.setState({
        isReady: true,
        directory
      })
    } catch (error) {
      this.setState({error})
    }
  }

  render () {
    const {directory, isReady} = this.state

    if (isReady) {
      return (
        <Page {...{isReady}}>
          <div className={theme.default}>
            {directory.isEmpty ?
              <h2>Folder is empty</h2> :
              <ul>{directory.children.map(file => <UploadedFile key={file.path.value} file={file}/>)}</ul>
            }
          </div>
        </Page>
      )
    }

    return <Page {...{isReady}}/>
  }
}

const mapStateToProps = ({files, status}) => ({files, status})
const dependencies = {
  client: 'client',
  controller: 'controller'
}

export default inject(connect(mapStateToProps)(Component), dependencies)
