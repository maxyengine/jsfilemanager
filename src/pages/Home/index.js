import React from 'react'
import { inject } from '@nrg/react-di'
import theme from './Home.module.scss'
import DragArea from '../../components/DragArea'
import StatusBar from '../../components/StatusBar'
import FileList from '../../components/FileList'
import Page from '../../components/Page'

const Component = class extends React.Component {

  state = {
    isReady: false,
    error: null
  }

  constructor (props) {
    super(props)
    this.controller = this.props.controller
  }

  async componentDidMount () {
    try {
      await this.controller.loadConfig()
      this.setState({isReady: true})
    } catch (error) {
      this.setState({error})
    }
  }

  render () {
    const {isReady, error} = this.state

    if (isReady) {
      return (
        <Page {...{isReady, error}}>
          <DragArea/>
          <StatusBar/>
          <FileList/>
        </Page>
      )
    }

    return <Page {...{isReady, error}}/>
  }
}

const dependencies = {controller: 'controller'}

export default inject(Component, dependencies)
