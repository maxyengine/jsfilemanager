const {Component} = require('@nrg/core')

module.exports = class extends Component {

  upload (file) {
    const http = new XMLHttpRequest()
    const body = new FormData()

    body.append('file', file)

    http.upload.onprogress = (...args) => this.trigger('progress', ...args)
    http.onerror = () => this.trigger('unknownError', http)
    http.onload = () => {
      if (200 <= http.status && http.status <= 299) {
        this.trigger('load')
      } else if (422 === http.status) {
        this.trigger('validationError', JSON.parse(http.response))
      } else if (400 === http.status) {
        this.trigger('domainError', JSON.parse(http.response))
      } else if (500 === http.status) {
        this.trigger('internalError', JSON.parse(http.response))
      } else {
        this.trigger('unknownError', JSON.parse(http.response))
      }
    }

    http.open('POST', this.url, true)

    for (const [key, value] of Object.entries(this.headers || {})) {
      http.setRequestHeader(key, value)
    }

    http.send(body)

    this.http = http
  }

  abort () {
    this.http.abort()
  }
}