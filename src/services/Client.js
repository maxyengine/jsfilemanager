import { Client } from '@nrg/http'

export default class extends Client {

  static get services () {
    return {
      ...Client.services,
      fileFactory: 'fileFactory'
    }
  }

  async fetchLogin (data) {
    return await this.post('/auth/login', data)
  }

  async fetchConfig () {
    return await this.post('/config')
  }

  async fetchUploadsFolder () {
    const raw = await this.post('/list', {path: 'uploads://'})

    return this.fileFactory.createDirectory(raw)
  }

  createFileUploader () {
    return super.createFileUploader('/upload')
  }

  openFile (fileName) {
    window.open(this.createUrl('/open', {fileName, Authorization: this.authorization()}), '_blank')
  }
}