import { Component } from '@nrg/core'

export default class extends Component {

  upload (file) {
    let loaded = 0
    const chunk = file.size / (Math.floor(Math.random() * (100 - 11) ) + 10)

    this.intervalId = setInterval(() => {
      loaded += chunk

      if (loaded <= file.size) {
        this.trigger('progress', {loaded})
      } else {
        clearInterval(this.intervalId)
        this.trigger('load')
      }
    }, 100)
  }

  abort () {
    clearInterval(this.intervalId)
  }
}