import { Value } from '@nrg/core'

export default class extends Value {

  get demoConfig () {
    return {
      maxSize: 209715200,
      denyExtensions: [
        'exe',
        'sh',
        'php'
      ]
    }
  }

  static get services () {
    return {
      uploaderFactory: 'uploaderFactory'
    }
  }

  fetchConfig () {
    return new Promise((resolve => {
      setTimeout(() => {resolve(this.demoConfig)}, 300)
    }))
  }

  createUploader () {
    return this.uploaderFactory.create()
  }

  openFile () {
    alert('This function is unavailable in demo mode. In live app it opens file in a new tab of a browser.')
  }
}