<?php

namespace Nrg\FileManager\Action\File;

use Nrg\FileManager\UseCase\File\UploadFile;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class UploadFileAction.
 *
 * Uploads a file to a path.
 */
class UploadFileAction implements Observer
{
    use ObserverStub;

    /**
     * @var UploadFile
     */
    private $uploadFile;

    /**
     * @param UploadFile $uploadFile
     */
    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    /**
     * Uploads a file to a path.
     *
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $params = $event->getRequest()->getBodyParams();
        $params['file'] = $event->getRequest()->getUploadedFiles()['file'];

        $event->getResponse()
            ->setBody($this->uploadFile->execute($params))
            ->setStatus(new HttpStatus(HttpStatus::CREATED));
    }
}
