<?php

namespace Nrg\Uploader\Action;

use DomainException;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Exception\ValidationException;
use Nrg\Http\Value\HttpStatus;
use Nrg\Uploader\Form\UploadFileForm;
use Nrg\Utility\Abstraction\Config;
use ReflectionException;

/**
 * Class UploadAction.
 *
 * Uploads a file to a path.
 */
class UploadAction
{
    /**
     * @var UploadFileForm
     */
    private $form;

    /**
     * @var string
     */
    private $uploadsFolder;

    /**
     * @param Config $config
     */
    public function __construct(UploadFileForm $form, Config $config)
    {
        $this->form = $form;
        $this->uploadsFolder = $config->get('uploadsFolder');
    }

    /**
     * Uploads a file to a path.
     *
     * @param HttpExchangeEvent $event
     *
     * @throws ReflectionException
     * @throws ValidationException
     */
    public function onNext($event)
    {
        $this->checkLastError();

        $this->form->populate($event->getRequest()->getUploadedFiles());

        if ($this->form->hasErrors()) {
            throw new ValidationException($this->form->getErrors());
        }

        $uploadedFile = $this->form->getElement('file')->getValue();
        $uploadedFile->moveTo($this->uploadsFolder.DIRECTORY_SEPARATOR.$uploadedFile->getName());

        $event->getResponse()->setStatusCode(HttpStatus::NO_CONTENT);
    }

    /**
     * Throws exception if the size of post data is greater than post_max_size
     *
     * @throws DomainException
     */
    private function checkLastError()
    {
        $error = error_get_last();

        if (null !== $error) {
            throw new DomainException($error['message']);
        }
    }
}
