<?php

namespace Nrg\Uploader\Action;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Exception\NotFoundException;
use Nrg\Utility\Abstraction\Config;

/**
 * Class OpenAction.
 *
 * Opens a file by a path.
 */
class OpenAction
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $uploadsFolder;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->uploadsFolder = $config->get('uploadsFolder');
    }

    /**
     * Opens a file by a path.
     *
     * @param HttpExchangeEvent $event
     *
     * @throws NotFoundException
     */
    public function onNext($event)
    {
        $fileName = $event->getRequest()->getQueryParam('fileName');
        $this->path = $this->uploadsFolder.DIRECTORY_SEPARATOR.$fileName;

        if (!is_file($this->path)) {
            throw new NotFoundException('File not found');
        }

        $event->getResponse()
            ->setHeader('Content-Type', mime_content_type($this->path).';charset=utf-8')
            ->setHeader('Content-Disposition', 'filename="'.$fileName.'"');
    }

    public function onComplete()
    {
        if (ob_get_level()) {
            ob_end_clean();
        }

        readfile($this->path);
    }
}
