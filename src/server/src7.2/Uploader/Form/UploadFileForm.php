<?php

namespace Nrg\Uploader\Form;

use Nrg\Form\Form;
use Nrg\I18n\Abstraction\Translator;
use Nrg\Uploader\Form\Element\UploadFileElement;

/**
 * Class UploadFileForm
 */
class UploadFileForm extends Form
{
    /**
     * @param Translator $translator
     * @param UploadFileElement $uploadFileElement
     */
    public function __construct(Translator $translator, UploadFileElement $uploadFileElement)
    {
        parent::__construct($translator);

        $this->addElement($uploadFileElement);
    }
}