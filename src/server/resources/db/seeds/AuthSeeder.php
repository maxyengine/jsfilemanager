<?php

use Phinx\Seed\AbstractSeed;

class AuthSeeder extends AbstractSeed
{
    const USER_TABLE = 'nrg_auth_user';
    const LOCALE = 'en_US';

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $userTable = $this->table(self::USER_TABLE);
        $faker = Faker\Factory::create(self::LOCALE);

        $userData = [
            'id' => $faker->uuid,
            'email' => 'admin@admin.com',
            'password' => password_hash('admin', PASSWORD_DEFAULT),
            'status' => 'active',
            'created' => date("Y-m-d H:i:s"),
        ];

        $userTable->insert($userData)->save();
    }
}
