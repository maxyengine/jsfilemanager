<?php

use Phinx\Migration\AbstractMigration;

/**
 * Class NrgAuth.
 */
class NrgAuth extends AbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function change()
    {
        $this->table('nrg_auth_user', ['id' => false])
            ->addColumn('id', 'uuid')
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('password', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('status', 'string', ['limit' => 20])
            ->addColumn('token', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated', 'timestamp', ['timezone' => true, 'null' => true])
            ->addIndex(['id'], ['unique' => true])
            ->addIndex(['id', 'token'], ['unique' => true])
            ->addIndex(['email'], ['unique' => true])
            ->create();
    }
}
