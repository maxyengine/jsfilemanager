<?php

use Nrg\Auth\Abstraction\AuthControl;
use Nrg\Auth\Action\RefreshLoginAction;
use Nrg\Auth\Action\LoginAction;
use Nrg\Auth\Middleware\Authorization;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Persistence\Repository\JsonUserRepository;
use Nrg\Auth\Service\JwtAuthControl;
use Nrg\FileManager\Action\Directory\ReadDirectoryAction;
use Nrg\FileManager\Persistence\Abstraction\FileRepository;
use Nrg\Http\Abstraction\ResponseEmitter;
use Nrg\Http\Abstraction\RouteProvider;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Middleware\AllowCors;
use Nrg\Http\Middleware\ErrorHandler;
use Nrg\Http\Middleware\ParseJsonRequest;
use Nrg\Http\Middleware\SerializeJsonResponse;
use Nrg\Http\Middleware\OffPrettyUrls;
use Nrg\Http\Middleware\EmitResponse;
use Nrg\Http\Middleware\RunAction;
use Nrg\Http\Service\HttpResponseEmitter;
use Nrg\Http\Service\HttpRouteProvider;
use Nrg\I18n\Abstraction\Translator;
use Nrg\I18n\Service\I18nTranslator;
use Nrg\Rx\Abstraction\EventProvider;
use Nrg\Rx\Service\RxEventProvider;
use Nrg\Uploader\Action\ConfigAction;
use Nrg\Uploader\Action\OpenAction;
use Nrg\Uploader\Action\UploadAction;
use Nrg\Uploader\Persistence\Repository\UploadedFileRepository;
use Nrg\Uploader\Service\AppConfig;
use Nrg\Utility\Abstraction\Config;

return [
    'routes' => [
        '/config' => ConfigAction::class,
        '/upload' => UploadAction::class,
        '/open' => OpenAction::class,
        '/list' => ReadDirectoryAction::class,
        '/auth/login' => LoginAction::class,
        '/auth/refresh' => RefreshLoginAction::class,
    ],
    'events' => [
        HttpExchangeEvent::class => [
            ErrorHandler::class,
            OffPrettyUrls::class,
            ParseJsonRequest::class,
            AllowCors::class,
            [
                Authorization::class,
                'freeAccessRoutes' => [
                    '/auth/login',
                ],
                'refreshAccessRoute' => '/auth/refresh',
            ],
            RunAction::class,
            SerializeJsonResponse::class,
            EmitResponse::class,
        ],
    ],
    'services' => [
        EventProvider::class => RxEventProvider::class,
        RouteProvider::class => HttpRouteProvider::class,
        ResponseEmitter::class => HttpResponseEmitter::class,
        Translator::class => I18nTranslator::class,
        AuthControl::class => JwtAuthControl::class,
        UserRepository::class => [
            JsonUserRepository::class,
            'path' => realpath(__DIR__.'/users.json'),
        ],
        Config::class => [
            AppConfig::class,
            'path' => realpath(__DIR__.'/../config.json'),
            'publicKeys' => [
                'maxSize',
                'denyExtensions',
                'allowExtensions',
            ],
        ],
        FileRepository::class => [
            UploadedFileRepository::class,
            'id' => 'uploads',
        ],
    ],
];
