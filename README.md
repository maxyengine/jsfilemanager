// Transpile to PHP 5.6
release\server\vendor\bin\php7to5 convert release\server\src7.2 dist\server\src

// Zip release
zip -r main.zip ./dev/ ./release/

// Migrations
.\vendor\bin\phinx migrate -c .\resources\db\phinx.yml
.\vendor\bin\phinx seed:run -c .\resources\db\phinx.yml
